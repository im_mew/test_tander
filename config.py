import os

GLOBAL_DIR = os.path.abspath(os.curdir)

DEBUG = False

TEMPLATE_DIR = '/templates/'

DATABASE_NAME = 'base'

HOST = ''
PORT = 8080
