from core.controller import MainView
from core.mysql_lib import db


class Statistic(MainView):
    """
    Вывод регионов, где комментариев больше 5
    """
    def get(self, request, *args, **kwargs):
        kwargs['stat'] = True
        kwargs['stat_main'] = True
        kwargs['regions'] = db.get_sql_data("""SELECT
            b.id as id,`name`,`count`
        FROM
            (SELECT
                `region`, COUNT(`region`) AS `count`
            FROM
                `comments`
            GROUP BY 
                `region`
            ) AS a
            INNER JOIN
                (SELECT
                    `id`, `name`
                FROM
                    `regions`
                ) AS b
            ON b.id=region
        WHERE `count`>5""")
        return super().get(request, *args, **kwargs)


class StatisticRegion(MainView):
    """
    Вывод городов региона с количеством комментариев в каждом городе региона
    """
    def get(self, request, *args, **kwargs):
        kwargs['stat'] = True
        kwargs['region'] = db.get_sql_data("SELECT * FROM `regions` WHERE id=%s", (kwargs['key'],))[0]
        kwargs['cites'] = db.get_sql_data("""SELECT
            `name`,`count`
        FROM
            (SELECT
                `city`, COUNT(`city`) AS `count`
            FROM
                `comments`
            GROUP BY 
                `city`
            ) AS a
            INNER JOIN
                (SELECT
                    `id`, `name`
                FROM
                    `citys`
                WHERE region=%s
                ) AS b
            ON b.id=city ORDER BY `count` DESC""", (kwargs['key'],))
        return super().get(request, *args, **kwargs)