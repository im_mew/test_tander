import re
from config import GLOBAL_DIR, TEMPLATE_DIR


def get_values(keystr, dictionary):
    """
    Получение переменных по строке из шаблона.
    :param keystr: строка
    :param dictionary: данные
    :return: данные в зависимости от типа переменных
    """
    keystr = keystr.split('.')
    for key in keystr:
        if isinstance(dictionary, dict):
            try:
                if key in dictionary:
                    dictionary = dictionary.get(key, '')
                elif int(key) in dictionary:
                    dictionary = dictionary.get(int(key), '')
            except ValueError:
                dictionary = ''
        elif isinstance(dictionary, (set, list,)):
            try:
                dictionary = dictionary[int(key)]
            except ValueError:
                dictionary = ''
    return dictionary


def for_parse(page, context):
    """
    Парсер FOR в шаблоне
    """
    matches = re.finditer(r"{% *for *(\w*\d*) *in *(\w*\d*) *%}([\s|\S]*?)({% *endfor *%})", page)
    for match in matches:
        block_text = ''
        if hasattr(get_values(match.group(2), context), "__iter__"):
            for item in get_values(match.group(2), context):
                matches_var = re.finditer(r"{{(.+?)}}", match.group(3))
                block_text += match.group(3)
                for match_var in matches_var:
                    block_text = re.sub(
                        r'{{ *%s *}}' % match_var.group(1),
                        str(get_values(''.join(match_var.group(1).strip().split('.')[1:]), item)), block_text)
        page = page.replace(match.group(), block_text)
    return page


def if_parse(page, context):
    """
    Парсер IF в шаблоне
    """
    regex = r"{% *if *(\d|\w+) *(==|<=|>=|<|>|\!=)? *(\'?\"?[\d|\w]+\'?\"?)? *%}([\s|\S]*?){% *endif *%}"
    matches = re.finditer(regex, page)
    for match in matches:
        block_text = ''
        if match.group(2) is None and match.group(3) is None:
            if get_values(match.group(1), context):
                block_text = match.group(4)

        page = page.replace(match.group(), block_text)
    return page


def template_parser(page, context):
    """
    Сборка шаблона
    """
    page = include_parse(page, context)
    page = for_parse(page, context)
    page = if_parse(page, context)
    matches = re.finditer(r"{{(.+?)}}", page)
    for match in matches:
        page = page.replace('{{%s}}' % match.group(1), str(get_values(match.group(1).strip(), context)))
    return page


def include_parse(page, context):
    """
    Парсер INCLUDE в шаблоне
    """
    matches = re.finditer(r"{% *include * [\'|\"](.*)[\'|\"] *%}", page)
    for match in matches:
        with open(GLOBAL_DIR + TEMPLATE_DIR + match.group(1), 'r', encoding="utf-8") as f:
            page = if_parse(page.replace(match.group(), f.read()), context)
    return page
