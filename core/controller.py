from urllib.parse import parse_qsl
from config import DEBUG, GLOBAL_DIR, TEMPLATE_DIR
from core.teplate_render import template_parser


def qs_parser(qs):
    """
    Приведение QS к виду словоря
    :param qs: строка
    :return: словарь
    """
    return {
        row[0]: row[1] for row in parse_qsl(qs)
    }


class MainView:
    """
    Майн класс-контроллер
    """
    context = None
    template = None

    def __init__(self, template=None):
        self.template = template

    def get(self, request, *args, **kwargs):

        # Если GET, то подгружаем шаблон
        with open(GLOBAL_DIR + TEMPLATE_DIR + self.template, 'r', encoding="utf-8") as file:
            return template_parser(file.read(), kwargs)

    def post(self, request, *args, **kwargs):
        pass

    def put(self, request, *args, **kwargs):
        pass

    def delete(self, request, *args, **kwargs):
        pass

    def views(self, request, *args, **kwargs):
        kwargs['DEBUG'] = DEBUG

        if request['REQUEST_METHOD'] == 'GET':
            request['GET'] = qs_parser(request['QUERY_STRING'])
            return self.get(request, *args, **kwargs)

        elif request['REQUEST_METHOD'] == 'POST':
            request['POST'] = qs_parser(request['wsgi.input'].read(
                int(request.get('CONTENT_LENGTH', 0))
            ).decode())
            return self.post(request, *args, **kwargs)

        elif request['REQUEST_METHOD'] == 'PUT':
            request['PUT'] = qs_parser(request['wsgi.input'].read(
                int(request.get('CONTENT_LENGTH', 0))
            ).decode())
            return self.put(request, *args, **kwargs)

        elif request['REQUEST_METHOD'] == 'DELETE':
            request['DELETE'] = qs_parser(request['wsgi.input'].read(
                int(request.get('CONTENT_LENGTH', 0))
            ).decode())
            return self.delete(request, *args, **kwargs)
