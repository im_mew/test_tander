import sqlite3
from config import GLOBAL_DIR, DATABASE_NAME


def dictfetchall(cursor):
    """
        Вывод словоря из курсора
    """
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


class BaseSQL:
    def __init__(self, base_name='default'):
        self.connect = sqlite3.connect(GLOBAL_DIR + '\\%s.sqlite' % (base_name,))
        self.init_base()

    def get_sql_data(self, sql, arr=None):
        """
        Получаем данные с MySQL
        :param sql: SQL запрос
        :param arr: параметры для запроса
        :return: данные славарём. Если данных нет, то None
        """
        cursor = self.connect.cursor()
        if arr:
            cursor.execute(sql % arr)
        else:
            cursor.execute(sql)

        result = dictfetchall(cursor)
        cursor.close()
        if len(result) != 0:
            return result

    def set_sql_data(self, sql, arr=None):
        cursor = self.connect.cursor()
        if arr:
            cursor.execute(sql % arr)
        else:
            cursor.execute(sql)

        self.connect.commit()
        cursor.close()
        return True

    def init_base(self):
        """
        Инициализация базы данных
        :return: None
        """
        self.set_sql_data("""CREATE TABLE if not exists comments (
                          `id` INTEGER PRIMARY KEY AUTOINCREMENT,
                          `last_name` VARCHAR(100) NOT NULL,
                          `first_name` VARCHAR(100) NOT NULL,
                          `middle_name` VARCHAR(100) NOT NULL,
                          `region` INTEGER NOT NULL,
                          `city` INTEGER NOT NULL,
                          `phone` VARCHAR(100) NOT NULL,
                          `email` VARCHAR(100) NOT NULL,
                          `text` TEXT NOT NULL)""")

        self.set_sql_data("""CREATE TABLE if not exists regions (
                          `id` INTEGER PRIMARY KEY AUTOINCREMENT,
                          `name` VARCHAR(100) NOT NULL)""")

        self.set_sql_data("""CREATE TABLE if not exists citys (
                          `id` INTEGER PRIMARY KEY AUTOINCREMENT,
                          `region` INTEGER NOT NULL,
                          `name` VARCHAR(100) NOT NULL)""")

        if not self.get_sql_data("SELECT `id` FROM `regions`"):
            self.set_sql_data("""INSERT INTO regions(`name`) VALUES ('Краснодарский край'),
                                                                    ('Ростовская Область'),
                                                                    ('Ставропольский Край');""")

            self.set_sql_data("""INSERT INTO citys(`name`, `region`) VALUES('Краснодар', 1),
                                                                           ('Кропоткин', 1),
                                                                           ('Славянск', 1),
                                                                           ('Ростов', 2),
                                                                           ('Шахты', 2),
                                                                           ('Батайск', 2),
                                                                           ('Ставрополь', 3),
                                                                           ('Пятигорск', 3),
                                                                           ('Кисловодск', 3);""")

    def __del__(self):
        self.connect.close()


db = BaseSQL(DATABASE_NAME)



