from config import DEBUG


def debug_print(content):
    """
    Простая функция для вывода на дебаге.
    :param content: Данные
    :return: None
    """
    if DEBUG:
        print(content)
