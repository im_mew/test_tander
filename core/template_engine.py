import re
from urls import patterns


def render(request, *args, **kwargs):
    """
    Получаем данные, в зависимости от запроса.
    :return: binary
    """
    result = ''

    for url in patterns:
        matches = re.finditer(url[0], request['PATH_INFO'])
        for match in matches:
            if match:
                kwargs.update(match.groupdict())
                result = url[1].views(request, *args, **kwargs)

                return [result.encode()]

    return [b'']
