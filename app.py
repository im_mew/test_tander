from config import PORT, HOST
from core.template_engine import render


def app(request, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])
    return render(request)


if __name__ == '__main__':
    try:
        from wsgiref.simple_server import make_server
        httpd = make_server(HOST, PORT, app)
        print('Serving on port {}...'.format(PORT))
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('Goodbye.')
