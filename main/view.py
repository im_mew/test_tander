import json

from core.controller import MainView
from core.mysql_lib import db


class MainPage(MainView):
    """
    Главная страница
    """
    def get(self, request, *args, **kwargs):
        kwargs['home'] = True
        return super().get(request, *args, **kwargs)


class AddComment(MainView):
    """
    Добавление комментария
    """
    def get(self, request, *args, **kwargs):
        """
        Вывод формы для добавления комментария
        """
        kwargs['add'] = True
        kwargs['regions'] = db.get_sql_data("SELECT * FROM `regions`")
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Добавление
        """
        if len(request['POST'].get('last_name', '')) == 0 or \
                        len(request['POST'].get('first_name', '')) == 0 or \
                        len(request['POST'].get('text', '')) == 0:
            return json.dumps('error')

        if db.set_sql_data("""INSERT INTO comments(
          `last_name`,
          `first_name`,
          `middle_name`,
          `region`,
          `city`,
          `phone`,
          `email`,
          `text`
        ) VALUES (
          '%s',
          '%s',
          '%s',
          '%s',
          '%s',
          '%s',
          '%s',
          '%s'
        )""", (
                request['POST'].get('last_name', ''),
                request['POST'].get('first_name', ''),
                request['POST'].get('middle_name', ''),
                request['POST'].get('region', ''),
                request['POST'].get('city', ''),
                request['POST'].get('phone', ''),
                request['POST'].get('email', ''),
                request['POST'].get('text', ''),
        )):
            return json.dumps('ok')


class GetCites(MainView):
    """
    Получение городов по региону
    """
    def post(self, request, *args, **kwargs):
        if request['POST'].get('drop', '') == '0':
            return json.dumps('none')

        return json.dumps(db.get_sql_data("SELECT * FROM `citys` WHERE region='%s'", (request['POST'].get('drop'),)))


class AllComments(MainView):
    """
    Вывод всех комментариев
    """
    def get(self, request, *args, **kwargs):
        kwargs['view'] = True
        kwargs['comments'] = db.get_sql_data("""SELECT
              comments.id AS id,
              `last_name`,
              `last_name`,
              `first_name`,
              `middle_name`,
              regions.name AS region,
              citys.name AS city,
              `phone`,
              `email`,
              `text`
            FROM
              comments
            INNER JOIN regions ON comments.region=regions.id
            INNER JOIN citys ON comments.city=citys.id""")
        return super().get(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """
        Удаление комментария
        """
        db.set_sql_data("DELETE FROM comments WHERE `id`='%s'", (request['DELETE'].get('delete'),))
        return json.dumps('ok')
