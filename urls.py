"""
    ['регулярка для URL', класс-контроллер]
"""

from main.view import MainPage, AllComments, AddComment, GetCites
from statistics.view import Statistic, StatisticRegion

patterns = [
    [r'/view/', AllComments('comments.html')],
    [r'/comment/drop/', GetCites()],
    [r'/comment/', AddComment('main.html')],
    [r'/stat/(?P<key>[0-9]+)/', StatisticRegion('stat.html')],
    [r'/stat/', Statistic('stat.html')],
    [r'/', MainPage('main.html')],
]
